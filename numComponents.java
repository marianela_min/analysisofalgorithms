// Homework extra credit
// Marianela Crissman

import java.util.*;


public class numConnected {

	final public static int INF = 9999;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Queue<int[][]> myWq = new LinkedList<int[][]>();   // stores the W matrices

		// all connected
		int [][] W1 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },  // 
				{0, 0 , 10, 15,INF,INF, 12},  // 1
				{0, 10, 0 , 20,INF, 13, 9 },  // 2
				{0, 15, 20, 0 , 18, 8 ,INF},  // 3
				{0,INF,INF, 18, 0 ,INF, 10},  // 4
				{0,INF, 13, 8 ,INF, 0 , 8 },  // 5
				{0, 12, 9 ,INF, 10, 8 , 0 }}; // 6
		myWq.add(W1);
		
		// 6 components		
		int [][] W2 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF,INF,INF,INF,INF},
				{0,INF, 0 ,INF,INF,INF,INF},
				{0,INF,INF, 0 ,INF,INF,INF},
				{0,INF,INF,INF, 0 ,INF,INF},
				{0,INF,INF,INF,INF, 0 ,INF},
				{0,INF,INF,INF,INF,INF, 0 }};
		myWq.add(W2);
		//		 4 components
		int [][] W3 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF,INF,INF,INF, 20},
				{0,INF, 0 ,INF,INF,INF,INF},
				{0,INF,INF, 0 ,INF,INF,INF},
				{0,INF,INF,INF, 0 ,INF, 4 },
				{0,INF,INF,INF,INF, 0 ,INF},
				{0, 20,INF,INF, 4 ,INF, 0 }};		
		myWq.add(W3);

		// 		// 3 components
		//							1	2	3	4	5	6	7	8	9
		int [][] W4 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF,INF,INF,INF,200,INF,INF,INF},// 1
				{0,INF, 0 ,INF,INF,INF,INF,INF,INF, 4 },// 2
				{0,INF,INF, 0 ,INF,INF,INF,INF, 50,INF},// 3
				{0,INF,INF,INF, 0 ,INF, 4 ,INF,INF,INF},// 4
				{0,INF,INF,INF,INF, 0 ,INF, 5 ,INF,INF},// 5
				{0,200,INF,INF, 4 ,INF, 0 ,INF,INF,INF},// 6
				{0,INF,INF,INF,INF, 5 ,INF, 0 ,INF, 10},// 7
				{0,INF,INF, 50,INF,INF,INF,INF, 0 ,INF},// 8
				{0,INF, 4 ,INF,INF,INF,INF, 10,INF, 0 }};// 9
		myWq.add(W4);

		// 		// 2 components
		int [][] W5 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF,INF,INF,INF, 20},
				{0,INF, 0 ,INF,INF,INF,INF},
				{0,INF,INF, 0 , 10,100,INF},
				{0,INF,INF, 10, 0 ,INF, 4 },
				{0,INF,INF,100,INF, 0 ,INF},
				{0, 20,INF,INF, 4 ,INF, 0 }};		
		myWq.add(W5);

		//		// 2 components
		int [][] W6 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF, 1 ,INF,INF,INF},  // 1
				{0,INF, 0 ,INF, 2 ,INF,INF},  // 2
				{0, 1 ,INF, 0 ,INF, 4 ,INF},  // 3
				{0,INF, 2 ,INF, 0 ,INF, 5 },  // 4
				{0,INF,INF, 4 ,INF, 0 ,INF},  // 5
				{0,INF,INF,INF, 5 ,INF, 0 }}; // 6
		myWq.add(W6);
		
		//all connected
		int [][] W7 = {	{0, 0 , 0 , 0 , 0 , 0 , 0 },
				{0, 0 ,INF, 1 ,INF,INF,INF},  // 1
				{0,INF, 0 ,INF, 2 ,INF,INF},  // 2
				{0, 1 ,INF, 0 ,INF, 4 , 6 },  // 3
				{0,INF, 2 ,INF, 0 ,INF, 5 },  // 4
				{0,INF,INF, 4 ,INF, 0 ,INF},  // 5
				{0,INF,INF, 6 , 5 ,INF, 0 }}; // 6
		myWq.add(W7);

		int [][] W8 ={	{0,0,0,0,0,0,0,0},
				{0,0,2,2,2,2,2,2},
				{0,2,0,1,1,1,1,1},
				{0,2,1,0,1,1,1,1},
				{0,2,1,1,0,1,1,1},
				{0,2,1,1,1,0,1,1},
				{0,2,1,1,1,1,0,1},
				{0,2,1,1,1,1,1,0}};
		myWq.add(W8);

		int times=0;
		while(!myWq.isEmpty()) 
		{
			times++;
			System.out.println("test: "+times);
			int[][] W = myWq.remove(); 
			int n = W.length-1;
			printWeights(W);
			int num = numComponents(n,W);
			System.out.println("\tMy answer: "+((num==1)?"\tThere is 1 connected component.\n":"\tThere are "+num+" connected components.\n"));
		}
	}



	//implementation of methods for the test driver

	//methos printWeights: prints the weighted adjacency matrix of a graph
	public static void printWeights(int [][] W)
	{
		int n = W.length-1;
		System.out.print("\t W= \n");
		for(int y=1; y<=n;y++) 
		{
			System.out.print("\t\t[  ");
			for(int z=1; z<=n;z++)
			{
				if(W[y][z]==INF)
					System.out.print("\tINF");
				else
					System.out.print("\t"+W[y][z]);
			}
			System.out.println("\t]");
		}
	}

	// returns the number of connected components in the graph given by W[nxn], indexed from 1 to n
	public static int numComponents(int n, int [][] W)
	{
		//variables to keep track of vertices already used or added to a set
		Queue<Integer> myq = new LinkedList<Integer>();   // vertices that have been used or reached
		Queue<Integer> myq_s = new LinkedList<Integer>(); // vertices that have not been used or reached
		int s=1;

		//variables to run prims algorithm
		int min;
		int [] dist = new int[n+1];
		int [] nearest = new int[n+1];
		int [] mincost = new int[n+1];  //mincost[0] = {  0  , if all connected
		//												 -1  , if all not connected.



		myq_s.add(s);
		int counter = 0;
		System.out.println("\n\t-------------------------------------------------------");
		System.out.println("\tRunning numComponents in graph W...");

		do {
			s = myq_s.remove();
			System.out.print("\t\t vertex "+s+" in another set? ");
			if(!myq.contains(s))
			{
				System.out.print(" No, let's use it.");
				counter++;  myq.add(s);
				//------------------------------------------------------------------------------------
				// basically prims here with an extra array that keeps track of the infinities and costs
				int vnear = s;
				int pre_vnear = -1;		//saves the last vnear used.

				for(int i=1;i<=n;i++) //initializes all to the source
				{
					dist[i] = mincost[i] = W[vnear][i];
					nearest[i]=vnear;
				}

				for(int i=1; i<n ; i++) 
				{
					min = INF;

					for(int j=1; j<=n;j++) // find min weight diff than 0 on dist[], assign index to vnear
					{
						if(dist[j]<min && dist[j]!=0)
						{
							min = dist[j];	vnear = j;
						}
					}

					if (pre_vnear != vnear)	 			// protects mincost if vnear could not be updated.
						mincost[vnear] = dist[vnear]; 	// updates mincost
					dist[vnear]=0;  					// zero out cost in dist

					for(int j =1; j<=n;j++) // this one updates the weights on dist[]
					{
						if(dist[j]>W[vnear][j])
						{
							dist[j] = W[vnear][j];
							nearest[j] = vnear;
						}				
					}
					pre_vnear = vnear; // make sure my cost will not be overriden.
				}

				//------------------------------------------------------------------------------------
				System.out.print("\t\tSubgraph from vertex "+s+".  --->  Set of edges: { ");
				for(int k=1;k<=n;k++)
				{
					if(mincost[k] == INF)
					{
						if(!(myq.contains(k) || myq_s.contains(k)))
							myq_s.add(k);
					}
					else
					{
						if(nearest[k]!=k)
						{
							System.out.print(" {"+nearest[k]+","+k+"} ");
							if(!myq.contains(k))
								myq.add(k);
							if(!myq.contains(nearest[k]))
								myq.add(nearest[k]);
						}
					}
				}
				System.out.println(" }");
			}
			else
				System.out.print(" Yes, let's ignore it. \n");
		}while(!myq_s.isEmpty());

		System.out.println("\n\t-------------------------------------------------------");
		return counter;
	}


}
